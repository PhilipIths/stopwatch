//
//  AppDelegate.h
//  Stopwatch
//
//  Created by Stjernström on 2015-03-18.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

