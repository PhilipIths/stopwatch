//
//  ViewController.m
//  Stopwatch
//
//  Created by Stjernström on 2015-03-18.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *TimeDisplay;
@property (weak, nonatomic) IBOutlet UITextField *minText;
@property (weak, nonatomic) IBOutlet UITextField *secText;
@property (weak, nonatomic) IBOutlet UIButton *startPauseResumeButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property (nonatomic) NSTimer *timer;
@property (nonatomic) NSInteger minutes;
@property (nonatomic) NSInteger seconds;
@property (nonatomic) NSInteger path;

@end

@implementation ViewController

- (IBAction)startPauseResumeButton_Click:(id)sender {
    
    // When Start is pressed
    if (self.path == 1) {
        
        if (self.secText.text.length > 0 && [self.secText.text intValue] <= 60) {
            self.seconds = [self.secText.text intValue];
            
            if (self.minText.text.length > 0) {
                self.minutes = [self.minText.text intValue];
            }
            
            else {
                self.minutes = 0;
            }
            
            if (self.seconds >= 10) {
                self.TimeDisplay.text = [NSString stringWithFormat: @"%d:%d", self.minutes, self.seconds];
            }
            
            else {
                self.TimeDisplay.text = [NSString stringWithFormat: @"%d:0%d", self.minutes, self.seconds];
            }
            
            self.minText.text = nil;
            self.minText.enabled = NO;
            self.secText.text = nil;
            self.secText.enabled = NO;
            
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
            
            [self.startPauseResumeButton setTitle:@"Pause" forState:UIControlStateNormal];
            self.path = 2;
            self.resetButton.enabled = YES;
        }
    }
    
    // When Pause is pressed
    else if (self.path == 2) {
        
        [self.timer invalidate];
        [self.startPauseResumeButton setTitle:@"Resume" forState:UIControlStateNormal];
        self.path = 3;
    }
    
    // When Resume is pressed
    else if (self.path == 3) {
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
        [self.startPauseResumeButton setTitle:@"Pause" forState:UIControlStateNormal];
        self.path = 2;
    }
}

- (IBAction)resetButton_Click:(id)sender {
    
    [self reset];
}

- (void)countDown {
    
    self.seconds -= 1;
    
    if (self.minutes == 0 && self.seconds == 0) {
        
        [self reset];
        UIAlertView* mes = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:@"Time's up!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [mes show];
    }
    
    else if (self.seconds < 0) {
        self.minutes -= 1;
        self.seconds = 59;
    }
    
    if (self.seconds >= 10) {
        self.TimeDisplay.text = [NSString stringWithFormat: @"%d:%d", self.minutes, self.seconds];
    }
    
    else {
        self.TimeDisplay.text = [NSString stringWithFormat: @"%d:0%d", self.minutes, self.seconds];
    }
}

- (void)reset {
    [self.timer invalidate];
    self.TimeDisplay.text = @"0:00";
    [self.startPauseResumeButton setTitle:@"Start" forState:UIControlStateNormal];
    self.minText.enabled = YES;
    self.secText.enabled = YES;
    self.resetButton.enabled = NO;
    self.path = 1;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.path = 1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
